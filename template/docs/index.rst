{{ scaffold.project_name }}
{{ repeat("=", len(scaffold.project_name)) }}

{{ scaffold.project_description }}

.. toctree::
   :maxdepth: 1
   :caption: Contents

.. toctree::
   :maxdepth: 1
   :caption: Miscellaneous

   todo.rst
{% if scaffold.license != 'none' %}   license.rst{% endif %}
{{ "" }}
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
