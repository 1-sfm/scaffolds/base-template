# {{ scaffold.project_name }}

{{ scaffold.project_description }}

<!-- Add your content here -->

{% if scaffold.license | lower != "none" %}
## License

{% if scaffold.license == "MIT" %}
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
{% endif %}
{% endif %}


## Locations

{% if scaffold.documentation_url %}
  * Documentation: [{{ scaffold.documentation_url }}]({{ scaffold.documentation_url }})
{% endif %}
{% if scaffold.project_url != "" %}
  * Website: [{{ scaffold.project_url }}]({{ scaffold.project_url }})
{% endif %}
  * mk-scaffold: [https://gitlab.com/cappysan/mk-scaffold](https://gitlab.com/cappysan/mk-scaffold)
{{ "" }}
