# Scaffold base template

Scaffold template to generate a scaffold project.


## Use case

This basic template is used to generate a scaffold template project. It can also be used as a base for a generic project.


## Question set

**author:** Template author's name.

**email:** The author's email.

**project_name:** The project name.

**project_short_name:** The project short name if needed. Freeform, usually a slugified version of the project's name.

**project_description:** Project short description. Usually a one-liner.

**project_url:** The projet's URL.

**documentation_url**: The documentation's URL.

**version:** "Initial version number."

**license:** "License to use."


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Website: [https://gitlab.com/cappysan/scaffolds/scaffold-template](https://gitlab.com/cappysan/scaffolds/scaffold-template)
  * mk-scaffold: [https://gitlab.com/cappysan/mk-scaffold](https://gitlab.com/cappysan/mk-scaffold)
